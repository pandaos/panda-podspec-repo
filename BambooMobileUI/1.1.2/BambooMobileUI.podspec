#
# Be sure to run `pod lib lint bamboo-mobile-ui.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BambooMobileUI'
  s.version          = '1.1.2'
  s.summary          = 'UI Toolkit for Apple iOS for the Bamboo Video Platform'

  s.description      = <<-DESC
This is the fastest way to create a customized Bamboo iOS application.
A Complete UI Toolkit for Apple iOS for the Bamboo Video Platform.
Created by Panda O.S. www.panda-os.com
DESC

  s.homepage         = 'https://bitbucket.org/pandaos/bamboo-mobile-ui'
  s.author           = { 'Leon Gordin' => 'leon@panda-os.com' }
  s.source           = { :git => 'https://bitbucket.org/pandaos/bamboo-mobile-ui.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'bamboo-mobile-ui/Classes/**/*'
  
  # s.resource_bundles = {
  #   'bamboo-mobile-ui' => ['bamboo-mobile-ui/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'AsyncImageView'
  s.dependency 'NZCircularImageView'
  s.dependency 'AXRatingView'
  s.dependency 'iCarousel'
  s.dependency 'ReflectionView'
  s.dependency 'KalturaPlayerSDK'
  s.dependency 'REFrostedViewController'
  s.dependency 'PVPClient'
  s.dependency 'MBProgressHUD'
  s.dependency 'AFViewShaker'
  s.dependency 'TPKeyboardAvoiding'
  s.dependency 'Google/Analytics'
end
