#
# Be sure to run `pod lib lint BambooPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BambooPlayer'
  s.version          = '0.1.9'
  s.summary          = 'The Bamboo media player library for iOS, written in Objective-C.'

  s.description      = 'This is the official media player library for the Panda-OS Bamboo Video Platform. The player acts as an independent player, as well as part of a Bamboo app.'

  s.homepage         = 'https://bitbucket.org/pandaos/bamboo-player-objc'
  s.author           = { "Oren Kosto" => "oren@panda-os.com" }
  s.source           = { :git => 'https://bitbucket.org/pandaos/bamboo-player-objc.git', :tag => s.version.to_s }
  s.social_media_url = 'https://www.facebook.com/pandaopensource'

  s.ios.deployment_target = '8.0'

  s.source_files = 'BambooPlayer/Classes/**/*'
  s.resources = "BambooPlayer/Assets/**/*"

  s.frameworks = 'UIKit', 'AVFoundation'
#  s.vendored_frameworks = 'BambooPlayer/Frameworks/*'
  s.dependency 'PVPClient'
  s.dependency 'ASValueTrackingSlider'
  s.dependency 'AsyncImageView'
  s.dependency 'GVRSDK'
  s.dependency 'MZTimerLabel'
  s.dependency 'GoogleAnalytics'
  s.dependency 'GoogleAds-IMA-iOS-SDK', '~> 3.5.0'


  s.xcconfig = {'LD_RUNPATH_SEARCH_PATHS' => '$(inherited) @executable_path/Frameworks','CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'}
end
