Pod::Spec.new do |s|
  s.name             = "PVPClient"
  s.version          = "1.5.0"
  s.summary          = "The PVP Client library for iOS."

  s.description      = "This is the iOS client for the Panda-OS Bamboo Video Platform. With this library, any app can communicate with the PVP Server."

  s.homepage         = "https://bitbucket.org/pandaos/pvpclient-ios"
  s.author           = { "Oren Kosto" => "oren@panda-os.com" }
  s.source           = { :git => "https://bitbucket.org/pandaos/pvpclient-ios.git", :tag => s.version.to_s }
  s.social_media_url = 'https://www.facebook.com/pandaopensource'

  s.ios.deployment_target = '8.0'
  s.tvos.deployment_target = '9.0'

  non_arc_files = 'Pod/Classes/Vendor/KalturaClient/**/*', 'Pod/Classes/Vendor/ASIHTTPRequest/**/*'

  s.source_files = 'Pod/Classes/**/*'
  s.exclude_files = non_arc_files
  s.requires_arc = true

  s.subspec 'no-arc' do |sp|
    sp.requires_arc = false
    sp.source_files = non_arc_files
  end

  s.frameworks = 'UIKit', 'SystemConfiguration', 'MobileCoreServices'

  s.dependency 'Overcoat', '~> 4.0.0-beta.2'

  s.library = 'z', 'xml2', 'stdc++'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }
  s.xcconfig = { 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES'}

end
