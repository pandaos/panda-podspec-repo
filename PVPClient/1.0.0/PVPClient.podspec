Pod::Spec.new do |s|
  s.name             = "PVPClient"
  s.version          = "1.0.0"
  s.summary          = "The PVP Client library for iOS."

  s.description      = "This is the iOS client for the Panda Video Platform. With this library, any app can communicate with the PVP Server."

  s.homepage         = "https://bitbucket.org/pandaos/pvpclient-ios"
  s.author           = { "Oren Kosto" => "oren@panda-os.com" }
  s.source           = { :git => "https://bitbucket.org/pandaos/pvpclient-ios.git", :tag => s.version.to_s }
  s.social_media_url = 'https://www.facebook.com/pandaopensource'

  s.platform     = :ios, '7.0'

  s.source_files = 'Pod/Classes/**/*'
  s.requires_arc = true

  s.resource_bundles = {
    'PVPClient' => ['Pod/Assets/*.png']
  }

  s.frameworks = 'UIKit', 'Social'

  s.dependency 'RestKit'
  s.dependency 'PiwikTracker'
  s.dependency 'ASIHTTPRequest'

  s.library = 'z', 'xml2', 'stdc++'
  s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2',
                 'LIBRARY_SEARCH_PATHS' => '"$(SRCROOT)/../Pod/Classes/Vendor/KalturaClient"' }

end
