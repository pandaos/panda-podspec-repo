#
# Be sure to run `pod lib lint BambooLive.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "BambooLive"
  s.version          = "0.1.0"
  s.summary          = "The Bamboo Live client library for iOS."

  s.description      = "This is the iOS client for streaming live video on the Bamboo Video Platform."

  s.homepage         = "https://github.com/pandaos/bamboo-live-ios"
  s.author           = { "Oren Kosto" => "oren@panda-os.com" }
  s.source           = { :git => "https://bitbucket.org/pandaos/bamboo-live-ios.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://www.facebook.com/pandaopensource'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'BambooLive' => ['Pod/Assets/*.png']
  }

  s.ios.frameworks = 'UIKit', 'AVFoundation'

  s.default_subspec = 'Core'

  s.subspec 'Core' do |ss|

  end

  s.subspec 'Player' do |ss|
    ss.dependency 'KalturaPlayerSDK'
  end

  s.dependency 'PVPClient'
  s.dependency 'VideoCore'

end
